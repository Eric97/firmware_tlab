/**
 * @file preflightcheck_params.c
 * preflightcheck parameters.
 *
 */

#include <systemlib/param/param.h>


PARAM_DEFINE_INT32(PRECHK_EN, 1);

