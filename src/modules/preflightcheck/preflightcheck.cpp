/**
 * @file preflightcheck.cpp
 * preflightcheck routine
 *
 * @author Eric Weiye Wei <eric_weiye@live.com>
 * @author Doris Yingxin Zhang <doris_yingxin@live.com>
 */
#include <nuttx/config.h>
//#include <src/include/px4.h>
#include <functional>
#include <cstdio>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <math.h>
#include <poll.h>
#include <drivers/drv_hrt.h>
#include <arch/board/board.h>
#include <systemlib/rc_check.h>

#include <drivers/drv_led.h>
#include <drivers/drv_hrt.h>
#include <drivers/drv_tone_alarm.h>
#include <drivers/drv_mag.h>
#include <drivers/drv_gyro.h>
#include <drivers/drv_accel.h>
#include <drivers/drv_baro.h>
#include <drivers/drv_device.h>

#include <systemlib/err.h>
#include <systemlib/param/param.h>

#include <uORB/topics/manual_control_setpoint.h>
#include <uORB/topics/actuator_controls.h>
#include <uORB/topics/vehicle_rates_setpoint.h>
#include <uORB/topics/vehicle_attitude.h>
#include <uORB/topics/vehicle_control_mode.h>
#include <uORB/topics/actuator_armed.h>
#include <uORB/topics/parameter_update.h>
#include <uORB/topics/vehicle_local_position.h>
#include <uORB/topics/position_setpoint_triplet.h>
#include <uORB/topics/vehicle_global_velocity_setpoint.h>
#include <uORB/topics/vehicle_local_position_setpoint.h>
#include <uORB/topics/sensor_combined.h>
#include <uORB/topics/preflightcheck.h>
#include <uORB/topics/vehicle_status.h>

#include <systemlib/systemlib.h>
#include <mathlib/mathlib.h>
#include <lib/geo/geo.h>
#include <mavlink/mavlink_log.h>
//#include <platforms/px4_defines.h>

#define TILT_COS_MAX	0.7f
#define SIGMA			0.000001f
#define MIN_DIST		0.01f
#define TIMEOUT			180000000
#define CHECK_TIME		5.0f
#define CHECK_RANGE_XY  0.3f
#define CHECK_RANGE_Z   0.3f
/**
 * preflightcheck app start / stop handling function
 *
 * @ingroup apps
 */
extern "C" __EXPORT int preflightcheck_main(int argc, char *argv[]);

class PreflightCheck
{
public:
	/**
	 * Constructor
	 */
	PreflightCheck();

	/**
	 * Destructor, also kills task.
	 */
	~PreflightCheck();

	/**
	 * Start task.
	 *
	 * @return		OK on success.
	 */
	int		start();


private:

	bool		_task_should_exit;		/**< if true, task should exit */
	int		_control_task;			/**< task handle for task */
	int		_mavlink_fd;			/**< mavlink fd */

	int		_att_sub;				/**< vehicle attitude subscription */
	//int		_att_sp_sub;			/**< vehicle attitude setpoint */
	int		_control_mode_sub;		/**< vehicle control mode subscription */
	int		_params_sub;			/**< notification of parameter updates */
	int		_manual_sub;			/**< notification of manual control updates */
	int		_arming_sub;			/**< arming status of outputs */
	int		_local_pos_sub;			/**< vehicle local position */
	int		_pos_sp_triplet_sub;	/**< position setpoint triplet */
	int		_v_status_sub;
	//int		_local_pos_sp_sub;		/**< offboard local position setpoint */
	//int		_global_vel_sp_sub;		/**< offboard global velocity setpoint */
	int		_sensor_sub;
	int 	i,r_err,p_err;
	int		mag_calid,mag_chkcal,acc_calid,acc_chkcal,acc_senerr,acc_no,mag_no,gyro_calid,gyro_chkcal,rc_cal;
	int32_t devid, calibration_devid;
	int ret;
	int fd;

	float avg_x,avg_y,avg_z;
	float avg_roll,avg_pitch,avg_yaw;

	orb_advert_t		_prechk_pub;

	struct vehicle_attitude_s			_att;			/**< vehicle attitude */
	struct manual_control_setpoint_s		_manual;		/**< r/c channel data */
	struct vehicle_control_mode_s			_control_mode;	/**< vehicle control mode */
	struct actuator_armed_s				_arming;		/**< actuator arming status */
	struct vehicle_local_position_s			_local_pos;		/**< vehicle local position */
	struct position_setpoint_triplet_s		_pos_sp_triplet;	/**< vehicle global position setpoint triplet */
	struct sensor_combined_s			_sensor;
	struct preflightcheck_s				_prechk;
	struct vehicle_status_s				_v_status;

	struct {
		param_t prechk_en;
	}		_params_handles;		/**< handles for interesting parameters */

	struct {
		int prechk_en;
	}		_params;

	/**
	 * Update our local parameter cache.
	 */
	int			parameters_update(bool force);

	/**
	 * Update control outputs
	 */
	void		control_update();

	/**
	 * Check for changes in subscribed topics.
	 */
	void		poll_subscriptions();
	

	bool 		Check_calibration();
	static void	task_main_trampoline(int argc, char *argv[]);

	/**
	 * Main sensor collection task.
	 */
	void		task_main();
};

namespace preflightcheck
{

/* oddly, ERROR is not defined for c++ */
#ifdef ERROR
# undef ERROR
#endif
static const int ERROR = -1;

PreflightCheck	*g_prechk;
}

PreflightCheck::PreflightCheck() :

	_task_should_exit(false),
	_control_task(-1),
	_mavlink_fd(-1),

/* subscriptions */
	_att_sub(-1),
	//_att_sp_sub(-1),
	_control_mode_sub(-1),
	_params_sub(-1),
	_manual_sub(-1),
	_arming_sub(-1),
	_local_pos_sub(-1),
	_pos_sp_triplet_sub(-1),
	_v_status_sub(-1),
	//_global_vel_sp_sub(-1),
	_sensor_sub(-1),
	i(0),
	r_err(0),
	p_err(0),
	mag_calid(0),
	mag_chkcal(0),
	acc_calid(0),
	acc_chkcal(0),
	acc_senerr(0),
	acc_no(0),
	mag_no(0),
	gyro_calid(0),
	gyro_chkcal(0),
	rc_cal(0),
	avg_x(0),
	avg_y(0),
	avg_z(0),
	avg_roll(0),
	avg_pitch(0),
	avg_yaw(0),
/* publications */
	_prechk_pub(-1)
{
	memset(&_att, 0, sizeof(_att));
	memset(&_manual, 0, sizeof(_manual));
	memset(&_control_mode, 0, sizeof(_control_mode));
	memset(&_arming, 0, sizeof(_arming));
	memset(&_local_pos, 0, sizeof(_local_pos));
	memset(&_pos_sp_triplet, 0, sizeof(_pos_sp_triplet));
	memset(&_sensor, 0 ,sizeof(_sensor));
	memset(&_prechk, 0 ,sizeof(_prechk));
	memset(&_v_status,0,sizeof(_v_status));

	_params_handles.prechk_en = param_find("PRECHK_EN");
	/* fetch initial parameter values */
	parameters_update(true);
}

PreflightCheck::~PreflightCheck()
{
	if (_control_task != -1) {
		/* task wakes up every 100ms or so at the longest */
		_task_should_exit = true;

		/* wait for a second for the task to quit at our request */
		unsigned i = 0;

		do {
			/* wait 20ms */
			usleep(20000);

			/* if we have given up, kill it */
			if (++i > 50) {
				task_delete(_control_task);
				break;
			}
		} while (_control_task != -1);
	}

	preflightcheck::g_prechk = nullptr;
}

int
PreflightCheck::parameters_update(bool force)
{
	bool updated;
	struct parameter_update_s param_upd;

	orb_check(_params_sub, &updated);

	if (updated) {
		orb_copy(ORB_ID(parameter_update), _params_sub, &param_upd);
	}

	if (updated || force) {
		param_get(_params_handles.prechk_en, &_params.prechk_en);

	}

	return OK;
}

void
PreflightCheck::poll_subscriptions()
{
	bool updated;

	orb_check(_att_sub, &updated);

	if (updated) {
		orb_copy(ORB_ID(vehicle_attitude), _att_sub, &_att);
	}

	orb_check(_control_mode_sub, &updated);

	if (updated) {
		orb_copy(ORB_ID(vehicle_control_mode), _control_mode_sub, &_control_mode);
	}

	orb_check(_manual_sub, &updated);

	if (updated) {
		orb_copy(ORB_ID(manual_control_setpoint), _manual_sub, &_manual);
	}

	orb_check(_arming_sub, &updated);

	if (updated) {
		orb_copy(ORB_ID(actuator_armed), _arming_sub, &_arming);
	}

	orb_check(_local_pos_sub, &updated);

	if (updated) {
		orb_copy(ORB_ID(vehicle_local_position), _local_pos_sub, &_local_pos);
	}
	
	orb_check(_sensor_sub, &updated);

	if (updated) {
		orb_copy(ORB_ID(sensor_combined), _sensor_sub, &_sensor);
	}

	orb_check(_v_status_sub,&updated);

	if(updated){
		orb_copy(ORB_ID(vehicle_status),_v_status_sub,&_v_status);
	}
}

void
PreflightCheck::task_main_trampoline(int argc, char *argv[])
{
	preflightcheck::g_prechk->task_main();
}

bool PreflightCheck::Check_calibration()
{
	/* ---- MAG ---- */
	fd = open(MAG_DEVICE_PATH, 0);
	if (fd < 0) {
	warn("failed to open magnetometer - start with 'hmc5883 start' or 'lsm303d start'");
	mavlink_log_critical(_mavlink_fd, "SENSOR FAIL: NO MAG");
	mag_no++;
	}

//	devid = ioctl(fd, DEVIOCGDEVICEID,0);
//	param_get(param_find("CAL_MAG0_ID"), &(calibration_devid));
//	if (devid != calibration_devid){
//	warnx("magnetometer calibration is for a different device - calibrate magnetometer first");
//	mavlink_log_critical(_mavlink_fd, "SENSOR FAIL: MAG CAL ID");
//	mag_calid++;
//	}

	ret = ioctl(fd, MAGIOCSELFTEST, 0);

	if (ret != OK) {
	warnx("magnetometer calibration missing or bad - calibrate magnetometer first");
	mavlink_log_critical(_mavlink_fd, "SENSOR FAIL: MAG CHECK/CAL");
	mag_chkcal++;
	}
	/* ---- ACCEL ---- */

	close(fd);
	fd = open(ACCEL_DEVICE_PATH, O_RDONLY);

//	devid = ioctl(fd, DEVIOCGDEVICEID,0);
//	param_get(param_find("CAL_ACC0_ID"), &(calibration_devid));
//	if (devid != calibration_devid){
//	warnx("accelerometer calibration is for a different device - calibrate accelerometer first");
//	mavlink_log_critical(_mavlink_fd, "SENSOR FAIL: ACC CAL ID");
//	acc_calid++;
//	}

	ret = ioctl(fd, ACCELIOCSELFTEST, 0);

	if (ret != OK) {
	warnx("accel self test failed");
	mavlink_log_critical(_mavlink_fd, "SENSOR FAIL: ACCEL CHECK/CAL");
	acc_chkcal++;
	}

	/* check measurement result range */
	struct accel_report acc;
	ret = read(fd, &acc, sizeof(acc));

	if (ret == sizeof(acc)) {
	/* evaluate values */
	if (sqrtf(acc.x * acc.x + acc.y * acc.y + acc.z * acc.z) > 30.0f /* m/s^2 */) {
		warnx("accel with spurious values");
		mavlink_log_critical(_mavlink_fd, "SENSOR FAIL: |ACCEL| > 30 m/s^2");
		acc_senerr++;
	}
	} else {
	warnx("accel read failed");
	mavlink_log_critical(_mavlink_fd, "SENSOR FAIL: ACCEL READ");
	acc_no++;;
	}
	/* ---- GYRO ---- */

	close(fd);
	fd = open(GYRO_DEVICE_PATH, 0);

//	devid = ioctl(fd, DEVIOCGDEVICEID,0);
//	param_get(param_find("CAL_GYRO0_ID"), &(calibration_devid));
//	if (devid != calibration_devid){
//	warnx("gyro calibration is for a different device - calibrate gyro first");
//	mavlink_log_critical(_mavlink_fd, "SENSOR FAIL: GYRO CAL ID");
//	gyro_calid++;
//	}

	ret = ioctl(fd, GYROIOCSELFTEST, 0);

	if (ret != OK) {
	warnx("gyro self test failed");
	mavlink_log_critical(_mavlink_fd, "SENSOR FAIL: GYRO CHECK/CAL");
	gyro_chkcal++;
	}
	/* ---- RC CALIBRATION ---- */

	bool rc_ok = (OK == rc_calibration_check(_mavlink_fd));

	/* warn */
	if (!rc_ok){
	warnx("rc calibration test failed");
	rc_cal++;
	}

	_prechk.mag_ok = true;
	_prechk.acc_ok = true;
	_prechk.gyro_ok = true;
	_prechk.rc_ok = true;

	if(r_err>0 || p_err>0 || mag_calid>0 || mag_chkcal>0 || mag_no>0 || acc_calid>0 || acc_chkcal>0 || acc_senerr>0 || acc_no>0 || gyro_calid>0 || gyro_chkcal>0 || rc_cal>0)
	{

		if(mag_calid>0|| mag_chkcal>0 || mag_no>0 )
		{
			_prechk.mag_ok = false;
		}
		if(acc_calid>0 || acc_chkcal>0 || acc_senerr>0 || acc_no>0 )
		{
			_prechk.acc_ok = false;
		}
		if(gyro_calid>0 || gyro_chkcal>0)
		{
			_prechk.gyro_ok = false;
		}
		if(rc_cal>0)
		{
			_prechk.rc_ok = false;
		//ioctl(buzzer, TONE_SET_ALARM, TONE_CHORD_G);
		//usleep(1000000);//check status every seconds
		}
		return false;
	}
	return true;

}



void
PreflightCheck::task_main()
{

	_mavlink_fd = open(MAVLINK_LOG_DEVICE, 0);
	usleep(2000000);	//standby for 10 seconds
	/*
	 * do subscriptions
	 */
	_att_sub = orb_subscribe(ORB_ID(vehicle_attitude));
	_control_mode_sub = orb_subscribe(ORB_ID(vehicle_control_mode));
	_params_sub = orb_subscribe(ORB_ID(parameter_update));
	_manual_sub = orb_subscribe(ORB_ID(manual_control_setpoint));
	_arming_sub = orb_subscribe(ORB_ID(actuator_armed));
	_local_pos_sub = orb_subscribe(ORB_ID(vehicle_local_position));
	_pos_sp_triplet_sub = orb_subscribe(ORB_ID(position_setpoint_triplet));
	_sensor_sub = orb_subscribe(ORB_ID(position_setpoint_triplet));
	_v_status_sub = orb_subscribe(ORB_ID(vehicle_status));

	bool calib_checked = false;
	hrt_abstime t = 0,t_prev = 0;
	int att_valid_count = 0;
	int gps_vel_valid_count = 0;
	bool gps_locked = false;
	hrt_abstime t_gps_locked = 0;

	parameters_update(true);


	/* initialize values of critical structs until first regular update */
	//_arming.armed = false;

	/* get an initial update for all sensor and status data */
	poll_subscriptions();

//	hrt_abstime t_prev = 0;

	/* wakeup source */


	int buzzer = open(TONEALARM_DEVICE_PATH, O_WRONLY);
	while (!_task_should_exit) {
		i = 1;
		usleep(20000);
//		if(i%50==0)
//			printf("[Precheck] On Going!\n");

		t  = hrt_absolute_time();
		float dt = t_prev > 0 ? (t - t_prev) / 1000000.0f : 0.0f;
		dt = fmaxf(fminf(0.01, dt), 0.1);		// constrain dt from 2 to 20 ms
		t_prev = t;

		poll_subscriptions();
		parameters_update(false);

		if(!_arming.armed){
		if(!calib_checked)
			{
				if(!Check_calibration())
					ioctl(buzzer, TONE_SET_ALARM, TONE_NOTIFY_NEGATIVE_TUNE);
				calib_checked = true;
			}


		if(_v_status.condition_local_position_valid)
		{
			if(gps_locked == false)
			{
				t_gps_locked = hrt_abstime();
			}
			gps_locked = true;


			avg_x += (_local_pos.x - avg_x) * dt / CHECK_TIME;
			float x_disp = _local_pos.x - avg_x;

			avg_y += (_local_pos.y - avg_y) * dt / CHECK_TIME;
			float y_disp = _local_pos.y - avg_y;

			avg_z += (_local_pos.z - avg_z) * dt / CHECK_TIME;
			float z_disp = _local_pos.z - avg_z;


//			if(i%50 == 0){
//				printf("[prechk] x_disp is %8.4lf\n",(double)x_disp);
//				printf("[prechk] y_disp is %8.4lf\n",(double)y_disp);
//				printf("[prechk] z_disp is %8.4lf\n",(double)z_disp);
//				printf("[prechk] t is %8.4lf\n",(double)t);
//				if(_v_status.condition_local_position_valid == true)
//				{
//					printf("[prechk] condition local pos valid!\n");
//				}
//				else
//				{
//					printf("[prechk] condition local pos not valid!\n");
//				}
//			}

			if(fabsf(_local_pos.vx) < 0.3f && fabsf(_local_pos.vy)<0.3f && fabsf(_local_pos.vz)<0.3f)
			{
				if(gps_vel_valid_count < 2*CHECK_TIME*50)
					gps_vel_valid_count++;

			}
			else
			{
				gps_vel_valid_count = 0;
			}


			if(t - t_gps_locked > CHECK_TIME*1000000 && fabsf(x_disp)<=CHECK_RANGE_XY && fabsf(y_disp)<=CHECK_RANGE_XY && fabsf(z_disp)<=CHECK_RANGE_Z && gps_vel_valid_count > 50 * CHECK_TIME)
			{
				_prechk.gps_ok = true;
			}
			else
			{
				_prechk.gps_ok = false;
			}
		}
		else
		{
			_prechk.gps_ok = false;
		}



		if(fabsf(_att.roll)<0.17f && fabsf(_att.pitch)<0.17f)
		{
			if(att_valid_count < 2*CHECK_TIME*50)
				att_valid_count++;
		}
		else
		{
			att_valid_count = 0;
		}


		avg_yaw += (_att.yaw - avg_yaw) * dt / CHECK_TIME;

//		if(i%50 == 0)
//			{
//				printf("att error is %8.4lf\n",(double)fabsf(avg_yaw - _att.yaw));
//				printf("att_valid_count is %d\n",att_valid_count);
//			}


		if(att_valid_count > CHECK_TIME * 50 && fabsf(avg_yaw - _att.yaw)<0.1f)
		{
			_prechk.att_ok = true;

		}
		else
		{
			_prechk.att_ok = false;
		}

		

		if(_prechk.acc_ok &&_prechk.gyro_ok && _prechk.mag_ok && _prechk.gps_ok &&_prechk.att_ok)
		{
			_prechk.pre_flight_check_passed = true;
		}


		if (_prechk_pub > 0) {
			orb_publish(ORB_ID(preflightcheck), _prechk_pub, &_prechk);

		} else {
			_prechk_pub = orb_advertise(ORB_ID(preflightcheck), &_prechk);
		}

		/* require RC ok to keep system_ok */
		//system_ok &= rc_ok;
		//ioctl(buzzer, TONE_SET_ALARM, TONE_SINGLE_BEEP_TUNE);
		}

	}
	_control_task = -1;
	_exit(0);
}

int
PreflightCheck::start()
{
	ASSERT(_control_task == -1);

	/* start the task */
	_control_task = task_spawn_cmd("preflightcheck",
				       SCHED_DEFAULT,
				       SCHED_PRIORITY_MAX - 5,
				       1600,
				       (main_t)&PreflightCheck::task_main_trampoline,
				       nullptr);

	if (_control_task < 0) {
		warn("task start failed");
		return -errno;
	}

	return OK;
}

int preflightcheck_main(int argc, char *argv[])
{
	if (argc < 1) {
		errx(1, "usage: preflightcheck {start|stop|status}");
	}

	if (!strcmp(argv[1], "start")) {

		if (preflightcheck::g_prechk != nullptr) {
			errx(1, "already running");
		}

		preflightcheck::g_prechk = new PreflightCheck;

		if (preflightcheck::g_prechk == nullptr) {
			errx(1, "alloc failed");
		}

		if (OK != preflightcheck::g_prechk->start()) {
			delete preflightcheck::g_prechk;
			preflightcheck::g_prechk = nullptr;
			err(1, "start failed");
		}

		exit(0);
	}

	if (!strcmp(argv[1], "stop")) {
		if (preflightcheck::g_prechk == nullptr) {
			errx(1, "not running");
		}

		delete preflightcheck::g_prechk;
		preflightcheck::g_prechk = nullptr;
		exit(0);
	}

	if (!strcmp(argv[1], "status")) {
		if (preflightcheck::g_prechk) {
			errx(0, "running");

		} else {
			errx(1, "not running");
		}
	}

	warnx("unrecognized command");
	return 1;
}
