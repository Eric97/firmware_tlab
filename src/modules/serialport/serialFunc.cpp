#include "serialFunc.h"

using namespace std;

#ifdef UAV_CPP

bool serialUAV::initialSerial(){
	init_counter += 1;
	if(init_counter>=10) printf("Number of failure in serial commmunication: %d\n", init_counter);

	USB = open(port.c_str(), O_RDWR);


	if (-1 == USB) {
		printf("Unable to open port!\n");
		status_port = false;
		return false;
	}

	struct termios tty;
	struct termios tty_old;
	memset (&tty, 0, sizeof(tty) );

	// Error Handling 
	if ( tcgetattr ( USB, &tty ) != 0 ){
		printf("Error from tcgetattr: %s\n", strerror(errno));
		return false;
	}

	// Save old tty parameters 
	tty_old = tty;

	// Set Baud Rate
	cfsetospeed (&tty, B115200);
	cfsetispeed (&tty, B115200);

	// Setting other Port Stuff
	tty.c_cflag &= ~(CS5 | CS6 | CS7 | HUPCL | PARENB | CSTOPB | CSIZE);
	tty.c_cc[VMIN] = 0;                  // read doesn't block
	tty.c_cc[VTIME] = 0;                  // 0.5 seconds read timeout
	tty.c_cflag |= (CS8 | CREAD | CLOCAL);     // turn on READ & ignore ctrl lines

	// Make raw
	tty.c_iflag = 0;
	tty.c_oflag = 0;
	tty.c_lflag  &= ~(ICANON | ECHO | ECHOE | ISIG);  //Input
	tty.c_oflag  &= ~OPOST;   //Output

	// Flush Port, then applies attributes 
	tcflush( USB, TCIOFLUSH );
	if ( tcsetattr ( USB, TCSANOW, &tty ) != 0){
		printf("Error from tcgetattr: %s\n", strerror(errno));
		return false;
	}

	return true;
}

int serialUAV::getWord(const vector<unsigned char> &rawData, int index){
    int lowBit = rawData.at(index);
    int highBit = rawData.at(index+1);
    int result = 256 * highBit + lowBit;
    return result;
}

void serialUAV::putWord(unsigned char *pointer, int data){
    unsigned char lowBit = data % 256;
    unsigned char highBit = data / 256;
    *pointer = lowBit;
    *(pointer+sizeof(unsigned char)) = highBit;
}

float serialUAV::getFloat(const vector<unsigned char> &rawData, int index){
    float result;
    unsigned char *pointer = (unsigned char *) &result;
    pointer[0] = rawData.at(index);
    pointer[1] = rawData.at(index+1);
    pointer[2] = rawData.at(index+2);
    pointer[3] = rawData.at(index+3);
    return result;

}

void serialUAV::putFloat(unsigned char *pointer, float data){
	unsigned char *p = (unsigned char *) &data;
	for(int i=0;i<4;++i){
		*(pointer + sizeof(unsigned char)*i) = p[i];
	}
}



int serialUAV::cutTillHeader(){
	// for debug
	/*
	cout<<"in cut till header, serialContainer size: "<<serialContainer.size()<<endl;
	for (std::list<unsigned char>::iterator iter = serialContainer.begin();iter != serialContainer.end(); ++iter) {
		cout << (int) *iter << ", ";
	}
	cout << endl;
	*/

	if(serialContainer.size()<3) return 0;

	for(std::list<unsigned char>::iterator iter=serialContainer.begin();iter!=----serialContainer.end();){


		std::list<unsigned char>::iterator iter_next = ++iter;
		std::list<unsigned char>::iterator iter_nnext = ++iter;
		----iter;

		if(*iter==Header_1 && *iter_next==Header_2 && *iter_nnext==Header_3){
			return 1;
		}
		else{
			//cout<<"intended to delete: "<<(int)*iter<<endl;
			iter = serialContainer.erase(iter);
		}
	}


	return 0;
}

int serialUAV::findEnding(std::list<unsigned char>::iterator &ending_iter){
	if(serialContainer.size()<3) return 0;
	for(std::list<unsigned char>::iterator iter=serialContainer.begin();iter!=----serialContainer.end();++iter){

		std::list<unsigned char>::iterator iter_next = ++iter;
		std::list<unsigned char>::iterator iter_nnext = ++iter;
		----iter;

		if (*iter == Ending_1 && *iter_next == Ending_2 && *iter_nnext == Ending_3) {
			ending_iter = iter;
			return 1;
		}
	}

	return 0;
}


void serialUAV::processRawData(){
	int contentCharLength = rawData.size()-2; //cout<<"content char length: "<<contentCharLength<<endl;
	//unsigned char *contentChar = new unsigned char(contentCharLength);
	unsigned char contentChar[2*BufferSize];

	int originCRC = getWord(rawData, contentCharLength);
	for(int i=0;i<contentCharLength;++i){
		//cout<<(int)rawData.at(i)<<", ";
		contentChar[i] = rawData.at(i);
	}
	//cout<<endl;
	crcInit();
	int calcCRC = crcFast(&contentChar[0], contentCharLength);
	//int calcCRC = 0;

	//cout<<"original crc: "<<originCRC<<"   calculated CRC: "<<calcCRC<<endl;

	if(calcCRC==originCRC && (contentCharLength-2)%4==0){
		// set the datalink status
		status_receive = true;

		// clear the received msg.
		receive.parameters.clear();

		receive.cmd = getWord(rawData, 0);
		for(int i=2;i<contentCharLength;i+=4){
			float decoded = getFloat(rawData, i);
			receive.parameters.push_back(decoded);
		}

	}
	else
		status_receive = false;


	//delete contentChar;

}

void serialUAV::readMsgFromSerial(){
	//if(!status_port) status_port = initialSerial();

	int	readStatus = read(USB, readBuff, BufferSize);
	if (readStatus < 0) {
		cout << endl << "Read fail!" << endl;
		receive.cmd = 0;
		receive.parameters.clear();
		status_receive = false;
		status_port = false;
	}
	else if (readStatus == 0) {
		//cout << endl << "Read nothing." << endl;
		receive.cmd = 0;
		receive.parameters.clear();
		status_receive = true;
		status_port = true;
	}
//	int readStatus = 36;
//	if(0){
//
//	}
	else{
		status_receive = true;
		status_port = true;
		for(int i=0;i<readStatus;++i){
			serialContainer.push_back(readBuff[i]);
		}

		int is_hasHeader = cutTillHeader();
		std::list<unsigned char>::iterator ending_iter;
		int is_hasEnding = findEnding(ending_iter);
		while(is_hasHeader==1 && is_hasEnding==1){
			//cout<<"get a message."<<endl;
			// put a message into rawData
			rawData.clear();
			for(std::list<unsigned char>::iterator iter=++++++serialContainer.begin();iter!=ending_iter;++iter){
				rawData.push_back(*iter);
			}

			// process raw data
			processRawData();

			// search for another message.
			serialContainer.erase(serialContainer.begin(), ending_iter);
			is_hasHeader = cutTillHeader();
			is_hasEnding = findEnding(ending_iter);
		}


	}

	// print the remain data in serialContainer
	/*
	for(std::list<unsigned char>::iterator iter=serialContainer.begin();iter!=serialContainer.end();++iter){
		cout<<(int)*iter<<", ";
	}
	cout<<endl;
	 */
}


int serialUAV::write2Serial(unsigned char *dataWrite, int writeLength){
	if(!status_port) status_port = initialSerial();

	int writeStatus = write(USB, dataWrite, writeLength);
	if(writeStatus<0){
		cout<<"Write fail."<<endl;
		status_send = false;
		status_port = false;
		return 0;
	}
	else{
		//cout<<"Write successful. Length: "<<writeStatus<<endl;
		status_send = true;
		status_port = true;
		return 1;
	}
}

int serialUAV::writeMsg2Serial(){
	int paraCharLength = 4*send.parameters.size();
	int charLength = 3 +   2 + paraCharLength + 2   + 3;

	unsigned char *dataWrite = new unsigned char(charLength);
	dataWrite[0] = Header_1;
	dataWrite[1] = Header_2;
	dataWrite[2] = Header_3;

	dataWrite[charLength-3] = Ending_1;
	dataWrite[charLength-2] = Ending_2;
	dataWrite[charLength-1] = Ending_3;

	putWord(&dataWrite[3], send.cmd);

	for(int i=0;i<send.parameters.size();++i){
		putFloat(&dataWrite[5+4*i], send.parameters.at(i));
	}


	// put CRC value
	int contentCharLength = 2 + paraCharLength;
	unsigned char *contentChar = new unsigned char(contentCharLength);
	for (int i = 0; i < contentCharLength; ++i) {
		contentChar[i] = dataWrite[3+i];
	}
	crcInit();
	int calcCRC = crcFast(contentChar, contentCharLength);
	putWord(&dataWrite[charLength-5], calcCRC);


	write2Serial(dataWrite, charLength);


	delete dataWrite;


	return status_send;
}




#else


bool serialUAV::initialSerial(){
	init_counter += 1;
	if(init_counter>=10) printf("Number of failure in serial commmunication: %d\n", init_counter);

	//for pixhawk default open
	int speed = B115200;
	//char uart_name

	int _uart_fd = open(port, O_RDWR | O_NOCTTY | O_NDELAY);

	if (_uart_fd < 0) {
		printf("open failed----------------------------\n");
		return false;
	}

	/* Try to set baud rate */
	struct termios uart_config;
	int termios_state;

	/* Fill the struct for the new configuration */
	tcgetattr(_uart_fd, &uart_config);

	uart_config.c_cflag |= (CLOCAL | CREAD);

	/* Clear ONLCR flag (which appends a CR for every LF) */
	uart_config.c_oflag &= ~ONLCR;
	uart_config.c_cc[VTIME] = 0;
	uart_config.c_cc[VMIN] = 0;

	//set 8N1
	uart_config.c_cflag &= ~PARENB;
	uart_config.c_cflag &= ~CSTOPB;
	uart_config.c_cflag &= ~CSIZE;
	uart_config.c_cflag |= CS8;

	uart_config.c_lflag &= ~(ICANON | ECHO | ECHOE | ISIG); /* Raw Input Mode */

	/* Set baud rate */
	if (cfsetispeed(&uart_config, speed) < 0
			|| cfsetospeed(&uart_config, speed) < 0) {
		//warnx("ERR SET BAUD %s: %d\n", "/dev/ttyS2", termios_state);
		close (_uart_fd);
		printf("open failed1\n");
		return false;
	}

	if ((termios_state = tcsetattr(_uart_fd, TCSANOW, &uart_config)) < 0) {
		//warnx("ERR SET CONF %s\n", "/dev/ttyS2");
		close (_uart_fd);
		printf("open failed2\n");
		return false;
	}

	(void) tcgetattr(_uart_fd, &uart_config);
	//uart_config.c_cflag |= CRTS_IFLOW;
	(void) tcsetattr(_uart_fd, TCSANOW, &uart_config);

	printf("open ok++++++++++++++++++++++++++++++++++++++\n");
	USB = _uart_fd;

	return true;
}

int serialUAV::getWord(const unsigned char *rawData, int index){
    int lowBit = rawData[index];
    int highBit = rawData[index+1];
    int result = 256 * highBit + lowBit;
    return result;
}

void serialUAV::putWord(unsigned char *pointer, int data){
    unsigned char lowBit = data % 256;
    unsigned char highBit = data / 256;
//    *pointer = lowBit;
//    *(pointer+sizeof(unsigned char)) = highBit;
    pointer[0] = lowBit;
    pointer[1] = highBit;
}

float serialUAV::getFloat(const unsigned char *rawData, int index){
    float result;
    unsigned char *pointer = (unsigned char *) &result;
    pointer[0] = rawData[index];
    pointer[1] = rawData[index+1];
    pointer[2] = rawData[index+2];
    pointer[3] = rawData[index+3];
    return result;

}

void serialUAV::putFloat(unsigned char *pointer, float data){
	unsigned char *p = (unsigned char *) &data;
//	for(int i=0;i<4;++i){
//		*(pointer + sizeof(unsigned char)*i) = p[i];
//	}
	pointer[0] = p[0];
	pointer[1] = p[1];
	pointer[2] = p[2];
	pointer[3] = p[3];
}


int serialUAV::cutTillHeader(){

	if(serialContainerSize<3) return 0;

	int idx = BufferSize+1;
	int temp = serialContainerSize-3;
	for(int i=0;i<=temp;++i){
		if(serialContainer[i]==Header_1 && serialContainer[i+1]==Header_2 && serialContainer[i+2]==Header_3 ){
			idx = i;
			break;
		}
	}

	if(idx==BufferSize+1){
		serialContainerSize = 2;
		memmove(serialContainer, &serialContainer[serialContainerSize-2], 2);
		return 0;
	}
	else {
		serialContainerSize -= idx;
		memmove(serialContainer, &serialContainer[idx], serialContainerSize);

		return 1;
	}
}

int serialUAV::findEnding(int &index){
	if(serialContainerSize<3) return 0;

	int temp = serialContainerSize-3;
	for(int i=0;i<=temp;++i){
		if(serialContainer[i]==Ending_1 && serialContainer[i+1]==Ending_2 && serialContainer[i+2]==Ending_3){
			index = i;
			return 1;
		}
	}

	return 0;
}


void serialUAV::processRawData(){
	int contentCharLength = rawDataSize-2; //cout<<"content char length: "<<contentCharLength<<endl;

	int originCRC = getWord(rawData, contentCharLength);

	int calcCRC = crcFast(&rawData[0], contentCharLength);
	//int calcCRC = 0;

	//cout<<"original crc: "<<originCRC<<"   calculated CRC: "<<calcCRC<<endl;

	if(calcCRC==originCRC && (contentCharLength-2)%4==0){
		// set the datalink status
		status_receive = true;

		// clear the received msg.
		receive.parameters.clear();

		receive.cmd = getWord(rawData, 0);
		for(int i=2;i<contentCharLength;i+=4){
			float decoded = getFloat(rawData, i);
			receive.parameters.push_back(decoded);
		}

		receive_history.push_back(receive);

//		if(receive.cmd==44){
//			int dataL = receive.parameters.size();
//			printf("received ref.....................size: %d\n", dataL);
//			printf("first value: %d\n", (int)receive.parameters.at(0));
//			printf("last value: %d\n", (int)receive.parameters.at(dataL-1));
//		}
//		if(receive.cmd==51) printf("received gps req.....................\n");

	}
	else
		status_receive = false;


}

void serialUAV::readMsgFromSerial(){
	receive.cmd = 0;
	receive_history.clear();

	int readStatus = read(USB, readBuff, BufferSize);
//	cout<<"read number: "<<readStatus<<endl;
//	for(int i=0;i<readStatus;++i){
//		cout<<(int)readBuff[i]<<", ";
//	}
//	cout<<endl;

	if (readStatus < 0) {
		//printf("\nRead fail (or nothing)!\n");
		receive.cmd = 0;
		receive.parameters.clear();
	}
	else if (readStatus == 0) {
		//printf("\nRead nothing.\n");
		receive.cmd = 0;
		receive.parameters.clear();
		status_receive = true;
		status_port = true;
	}
//	int readStatus = 18;
//	if(0);
	else{
		status_receive = true;
		status_port = true;

		if(serialContainerSize>=BufferSize-readStatus) serialContainerSize = 0;
		memcpy(&serialContainer[serialContainerSize], readBuff, readStatus);
		serialContainerSize+=readStatus;

		int is_hasHeader = cutTillHeader();
		int ending_idx = 0;
		int is_hasEnding = findEnding(ending_idx);
		while(is_hasHeader==1 && is_hasEnding==1){
			//printf("get a message.\n");
			// put a message into rawData
			rawDataSize = ending_idx-3;
			if(rawDataSize>ContentLength) rawDataSize = ContentLength;
			memcpy(rawData, &serialContainer[3], rawDataSize);


			// process raw data
			processRawData();

			// search for another message.
			serialContainerSize -= ending_idx;
			memmove(serialContainer, &serialContainer[ending_idx], serialContainerSize);

			is_hasHeader = cutTillHeader();
			is_hasEnding = findEnding(ending_idx);
		}

		//printf("container size: %d\n", serialContainerSize);



	}

	// print the remain data in serialContainer

//	for(int i=0;i<serialContainerSize;++i){
//		cout<<(int)serialContainer[i]<<", ";
//	}
//	cout<<endl;

}


int serialUAV::write2Serial(const unsigned char *dataWrite, int writeLength){
	if(!status_port) status_port = initialSerial();
//	for(int i=0;i<writeLength;++i){
//		cout<<std::hex<<(int)dataWrite[i]<<", ";
//	}
//	cout<<endl;

	int writeStatus = write(USB, dataWrite, writeLength);
	if(writeStatus<0){
		//printf("write fail.\n");
		status_send = false;
		status_port = false;

		return 0;
	}
	else{
		//cout<<"Write successful. Length: "<<writeStatus<<endl;
		status_send = true;
		status_port = true;
		return 1;
	}
}

int serialUAV::writeMsg2Serial(){


	int paraCharLength = 4*send.parameters.size();
	int charLength = 3 +   2 + paraCharLength + 2   + 3;


	dataWrite[0] = Header_1;
	dataWrite[1] = Header_2;
	dataWrite[2] = Header_3;

	dataWrite[charLength-3] = Ending_1;
	dataWrite[charLength-2] = Ending_2;
	dataWrite[charLength-1] = Ending_3;

	putWord(&dataWrite[3], send.cmd);

	for(int i=0;i<send.parameters.size();++i){
		putFloat(&dataWrite[5+4*i], send.parameters.at(i));
	}



	int calcCRC = crcFast(&dataWrite[3], 2 + paraCharLength);
	putWord(&dataWrite[charLength-5], calcCRC);


	write2Serial(dataWrite, charLength);



	return status_send;
}


#endif


// for crc -------------------
void serialUAV::crcInit(void)
{
    crc			   remainder;
	int			   dividend;
	unsigned char  bit;

	for (dividend = 0; dividend < 256; ++dividend) {
		remainder = dividend << (WIDTH - 8);

		for (bit = 8; bit > 0; --bit) {
			if (remainder & TOPBIT) {
				remainder = (remainder << 1) ^ POLYNOMIAL;
			} else {
				remainder = (remainder << 1);
			}
		}

		crcTable[dividend] = remainder;
    }

}

crc serialUAV::crcFast(unsigned char const message[], int nBytes) {
	crc remainder = INITIAL_REMAINDER;
	unsigned char data;
	int byte;

	for (byte = 0; byte < nBytes; ++byte) {
		data = (message[byte]) ^ (remainder >> (WIDTH - 8));
		remainder = crcTable[data] ^ (remainder << 8);
	}

	return ( (remainder) ^ FINAL_XOR_VALUE);

}


serialUAV serialdata((char*)"/dev/ttyS1");
