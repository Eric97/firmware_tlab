/*
 * preflightcheck.h
 *
 *  Created on: Oct 28, 2014
 *      Author: Administrator
 */

#ifndef TOPIC_PREFLIGHTCHECK_H_
#define TOPIC_PREFLIGHTCHECK_H_

#include <stdint.h>
#include "../uORB.h"

/**
 * @addtogroup topics
 * @{
 */
/*
	USE DIGIT AS PREFLIGHTCHECK DIFFERENT CONDITIONS:
		1-X-X-X-X, WHEN ATT ERROR
		X-1-X-X-X, WHEN MAG ERROR
		X-X-1-X-X, WHEN ACC ERROR
		X-X-X-1-X, WHEN GYRO ERROR
		X-X-X-X-1, WHEN RC CHAN 1~4 NEVER CALIBRATED
	Conditions could happen together so that the digit may be multiplex combination such as 11000, 11111 etc.
*/
struct preflightcheck_s {
	uint16_t preflightcheck;
	bool pre_flight_check_passed;
	bool acc_ok;
	bool gyro_ok;
	bool mag_ok;
	bool att_ok;
	bool gps_ok;
	bool rc_ok;
};

/**
 * @}
 */

/* register this as object request broker structure */
ORB_DECLARE(preflightcheck);

#endif /* SYS_STATS_H_ */
