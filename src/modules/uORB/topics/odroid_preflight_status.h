/**
hailong
 */

#ifndef ODROID_PREFLIGHT_STATUS_H_
#define ODROID_PREFLIGHT_STATUS_H_

#include <stdint.h>
#include <stdbool.h>
#include "../uORB.h"



typedef enum {
	ODROID_PREFLIGHT_STATE_AUTO = 0,
	ODROID_PREFLIGHT_STATE_INIT,
	ODROID_PREFLIGHT_STATE_ACTION,
} odroid_preflight_state_t;


struct odroid_preflight_status_s {

	odroid_preflight_state_t odroid_preflight_state;		/**< set odroid navigation state */


};

/**
 * @}
 */

/* register this as object request broker structure */
ORB_DECLARE(odroid_preflight_status);

#endif
