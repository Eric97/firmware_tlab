/*
 * continue_mission.h
 *
 *  Created on: Oct 27, 2014
 *      Author: NUS_UAV
 */

#ifndef CONTINUE_WAYPOINT_H_
#define CONTINUE_WAYPOINT_H_

#include "../uORB.h"

/**
 * @addtogroup topics
 * @{
 */

struct continue_waypoint_s {
	uint64_t t_timestamp;
     bool  continue_mission;
}; /**< Velocity setpoint in NED frame */

/**
 * @}
 */

/* register this as object request broker structure */
ORB_DECLARE(continue_waypoint);



#endif /* CONTINUE_MISSION_H_ */
