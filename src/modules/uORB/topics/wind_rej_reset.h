/*
 * wind_rej_height_reset.h
 *
 *  Created on: Jun 13, 2015
 *      Author: NUS_UAV
 */

#ifndef WIND_REJ_RESET_H_
#define WIND_REJ_RESET_H_

#include "../uORB.h"

/**
 * @addtogroup topics
 * @{
 */

struct wind_rej_reset_s {
	uint64_t t_timestamp;
	bool reset_height;
	float throttle_sp;
	float roll_sp;
	float yaw_sp;
	float pitch_sp;
}; /**< Velocity setpoint in NED frame */

/**
 * @}
 */

/* register this as object request broker structure */
ORB_DECLARE(wind_rej_reset);



#endif /* WIND_REJ_HEIGHT_RESET_H_ */
