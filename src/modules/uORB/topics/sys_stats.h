/*
 * sys_stats.h
 *
 *  Created on: Oct 28, 2014
 *      Author: Administrator
 */

#ifndef TOPIC_SYS_STATS_H_
#define TOPIC_SYS_STATS_H_

#include <stdint.h>
#include "../uORB.h"

/**
 * @addtogroup topics
 * @{
 */

struct sys_stats_s {
//	uint64_t timestamp;		///< in microseconds since system start
	uint16_t onboard_time;	///< in seconds since system on power
	uint16_t flight_time;	///< in seconds with accumulated time
	float total_flight_min;	///< total accumulative flight time in air in minutes
	float disk_usage;		///< SD card usage
};

/**
 * @}
 */

/* register this as object request broker structure */
ORB_DECLARE(sys_stats);

#endif /* SYS_STATS_H_ */
