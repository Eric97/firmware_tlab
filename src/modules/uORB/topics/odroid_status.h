/**
hailong
 */

#ifndef ODROID_STATUS_H_
#define ODROID_STATUS_H_

#include <stdint.h>
#include <stdbool.h>
#include "../uORB.h"



typedef enum {
	ODROID_STATE_AUTO = 0,
	ODROID_STATE_TAKEOFF,                         /**odroid takeoff */
	ODROID_STATE_ACTION,
	ODROID_STATE_LAND		/**< odroid land */
} odroid_state_t;


struct odroid_status_s {

	odroid_state_t odroid_nav_state;		/**< set odroid navigation state */


};

/**
 * @}
 */

/* register this as object request broker structure */
ORB_DECLARE(odroid_status);

#endif
